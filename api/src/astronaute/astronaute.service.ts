import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { Astronaute, Prisma } from '@prisma/client';

@Injectable()
export class AstronauteService {
  constructor(private prisma: PrismaService) {}

  async findOne(
    astronauteWhereUniqueInput: Prisma.AstronauteWhereUniqueInput,
  ): Promise<Astronaute | null> {
    return this.prisma.astronaute.findUnique({
      where: astronauteWhereUniqueInput,
    });
  }

  async findAll(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.AstronauteWhereUniqueInput;
    where?: Prisma.AstronauteWhereInput;
    orderBy?: Prisma.AstronauteOrderByWithRelationInput;
  }): Promise<Astronaute[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.astronaute.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createOne(data: Prisma.AstronauteCreateInput): Promise<Astronaute> {
    return this.prisma.astronaute.create({
      data,
    });
  }

  async updateOne(params: {
    where: Prisma.AstronauteWhereUniqueInput;
    data: Prisma.AstronauteUpdateInput;
  }): Promise<Astronaute> {
    const { where, data } = params;
    return this.prisma.astronaute.update({
      data,
      where,
    });
  }

  async deleteOne(where: Prisma.AstronauteWhereUniqueInput): Promise<Astronaute> {
    return this.prisma.astronaute.delete({
      where,
    });
  }
}