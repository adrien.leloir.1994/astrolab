import { Module } from '@nestjs/common';
import { AstronauteController } from './astronaute.controller';
import { AstronauteService } from './astronaute.service';
import { PrismaService } from 'src/prisma.service';

@Module({
  imports: [],
  controllers: [AstronauteController],
  providers: [AstronauteService, PrismaService]
})
export class AstronauteModule { }
