import { ApiProperty } from "@nestjs/swagger";
import { IsEmpty, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from "class-validator"
import { AstronauteDTO } from '@shared'

export class AstronauteCreateDTO implements AstronauteDTO {
    @IsEmpty()
    id: number

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(64)
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(256)
    favoriteCatColor: string
}

export class AstronauteUpdateDTO {
    @IsEmpty()
    id: number

    @ApiProperty()
    @IsOptional()
    @IsString()
    @MinLength(2)
    @MaxLength(64)
    name: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    @MinLength(2)
    @MaxLength(256)
    favoriteCatColor: string
}