import { Body, Controller, Delete, Get, Inject, Param, Patch, Post } from '@nestjs/common';
import { AstronauteService } from './astronaute.service';
import { AstronauteCreateDTO, AstronauteUpdateDTO } from './astronaute.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Astronaute')
@Controller('astronaute')
export class AstronauteController {
    constructor(private astronauteService: AstronauteService) { }

    @Get('/')
    get() {
        return this.astronauteService.findAll({})
    }

    @Get('/:id')
    getOne(@Param('id') id: number) {
        return this.astronauteService.findOne({ id: Number(id) })
    }

    @Post('/')
    create(@Body() astronauteDTO: AstronauteCreateDTO) {
        return this.astronauteService.createOne(astronauteDTO)
    }

    @Patch('/:id')
    update(@Param('id') id: string, @Body() astronauteDTO: AstronauteUpdateDTO) {
        return this.astronauteService.updateOne({ data: astronauteDTO, where: { id: Number(id) } })
    }

    @Delete('/:id')
    delete(@Param('id') id: string) {
        return this.astronauteService.deleteOne({ id: Number(id) })
    }
}
