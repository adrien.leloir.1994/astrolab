import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
    const usePort = 5000;

    const app = await NestFactory.create(AppModule);

    app.useGlobalPipes(new ValidationPipe());
    app.enableCors()

    // Configure swagger
    const config = new DocumentBuilder()
        .setTitle('Astrolab')
        .setDescription(`Gestion du personnel astronavigant pour les voyages galactiques. Sinon c'est la description de l'API`)
        .setVersion('1.0')
        .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    // Start to listening
    await app.listen(usePort, () => { console.log(`Listening on port ${usePort}`) });
}
bootstrap();
