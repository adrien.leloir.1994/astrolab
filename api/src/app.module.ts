import { Module } from '@nestjs/common';
import { AstronauteModule } from './astronaute/astronaute.module';

@Module({
    imports: [AstronauteModule],
    controllers: [],
    providers: []
})
export class AppModule {}
