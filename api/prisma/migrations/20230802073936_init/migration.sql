/*
  Warnings:

  - You are about to drop the `Test` table. If the table is not empty, all the data it contains will be lost.

*/

-- CreateTable
CREATE TABLE "Astronaute" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "favoriteCatColor" TEXT NOT NULL,

    CONSTRAINT "Astronaute_pkey" PRIMARY KEY ("id")
);
