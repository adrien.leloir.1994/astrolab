# Astrolab 

## Installation

- Clone repo
- Create database (PG)
- Edit .env file (set database config access)
- npm install
- npx prisma migrate dev
- npm run dev (client)
- npm run start:dev (api)
