import { createContext, useState, useEffect } from 'react';
import { AstronauteDTO } from '@shared';

export type DataContextType = {
    astronautes: AstronauteDTO[],
    deleteAstronaute: (id: number) => void,
    createAstronaute: (astronauteDTO: Omit<AstronauteDTO, 'id'>) => void,
    updateAstronaute: (id: number, astronauteDTO: Omit<AstronauteDTO, 'id'>) => void,
    isLoading: boolean,
    isError: boolean
}

const DataContext = createContext<DataContextType>({});

const DataProvider = ({ children }: { children: JSX.Element }) => {
    const [astronautes, setAstronautes] = useState<AstronauteDTO[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isError, setIsError] = useState<boolean>(false);

    useEffect(() => {
        fetchAstronautes();
    }, []);

    // Get all
    const fetchAstronautes = async () => {
        try {
            const response = await fetch('http://localhost:5000/astronaute/');

            if (!response.ok) {
                setIsError(true);

                console.error("Erreur lors de la récuperation")
            }

            const data = await response.json();

            setAstronautes(data);
            setIsLoading(false);
        } catch (err) {
            setIsError(true);

            console.error("Erreur lors de la récuperation", err)
        }
    };

    // Delete one
    const deleteAstronaute = async (id: number) => {
        try {
            const response = await fetch(`http://localhost:5000/astronaute/${id}`, { 'method': 'DELETE' });

            if (!response.ok) {
                setIsError(true);

                console.error("Erreur lors de la suppression")
            }

            setAstronautes(astronautes.filter((a) => a.id !== id))
        } catch (err) {
            setIsError(true);
            console.error("Erreur lors de la suppression", err)
        }
    };

    // Create one
    const createAstronaute = async (astronauteDTO: Omit<AstronauteDTO, 'id'>) => {
        try {
            const response = await fetch(`http://localhost:5000/astronaute`, {
                'method': 'POST',
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(astronauteDTO)
            });

            if (!response.ok) {
                setIsError(true);

                console.error("Erreur lors de la creation")
            }

            const data = await response.json();

            setAstronautes([...astronautes, data])
        } catch (err) {
            setIsError(true);
            console.error("Erreur lors de la création", err)
        }
    };

    // Update one
    const updateAstronaute = async (id: number, astronauteDTO: Omit<AstronauteDTO, 'id'>) => {
        try {
            const response = await fetch(`http://localhost:5000/astronaute/${id}`, {
                'method': 'PATCH',
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(astronauteDTO)
            });

            if (!response.ok) {
                setIsError(true);

                console.error("Erreur lors de l'update")
            }
            
            const data = await response.json();

            setAstronautes([...astronautes.filter((a) => a.id !== id), data])
        } catch (err) {
            setIsError(true);
            console.error("Erreur lors de l'update", err)
        }
    };

    return (
        <DataContext.Provider
            value={{
                astronautes,
                deleteAstronaute,
                createAstronaute,
                updateAstronaute,
                isLoading,
                isError
            }
            }>
            {children}
        </DataContext.Provider>
    );
};

export { DataProvider, DataContext };