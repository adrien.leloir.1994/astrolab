import Astronaute from '../astronauteManage/astronaute'
import './App.css'

const App = () => {
  return (
    <>
      <div>
        <h1>Welcome to Astrolab !</h1>
        <p>You have now access to manage intergalactic travel</p>
      </div>
      <Astronaute />
    </>
  )
}

export default App
