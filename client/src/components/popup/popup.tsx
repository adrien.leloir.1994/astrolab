import './popup.css';

export const Popup = ({ children, isOpen, closeHandler }: PopupProps) => {
    return !isOpen ? null :
        <div className="popup" >
            <div className="popup-content">
                {children}
                <button className="popup-button-close" onClick={closeHandler}>Close</button>
            </div>
        </div>
}

type PopupProps = {
    children: JSX.Element,
    isOpen: boolean,
    closeHandler: () => void
}