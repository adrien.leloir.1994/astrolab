import './astronaute.css'
import { useContext, useState } from 'react';
import { DataContext, DataContextType } from '../core/dataprovider';
import { Popup } from '../popup/popup';
import { AstronauteDTO } from '@shared';
import { AstronauteCreateForm } from './astronauteCreate';
import { AstronauteUpdateForm } from './astronauteUpdate';

const Astronaute = () => {
    const { astronautes, isLoading, isError, deleteAstronaute } = useContext<DataContextType>(DataContext);

    const [isPopupCreateOpen, setIsPopupCreateOpen] = useState(false);
    const [isPopupUpdateOpen, setisPopupUpdateOpen] = useState(false);
    const [updatingAstronauteId, setUpdatingAstronauteId] = useState<number | null>(null);

    // Handlers
    const closeCreatePopupHandler = () => {
        setIsPopupCreateOpen(false);
    };

    const openCreatePopupHandler = () => {
        setIsPopupCreateOpen(true);
    };

    const openUpdatePopupHandler = (astronauteId: number) => {
        setisPopupUpdateOpen(true);
        setUpdatingAstronauteId(astronauteId);
    };

    const closeUpdatePopupHandler = () => {
        setisPopupUpdateOpen(false);
        setUpdatingAstronauteId(null);
    };

    const deleteHandler = (astronauteId: number) => {
        deleteAstronaute(astronauteId)
    }

    if (isError) {
        return (<p>Oops error...</p>)
    }
    else if (isLoading) {
        return (<p>Loading list...</p>)
    }

    return (
        <>
            <ul className='astrolist-list'>
                {astronautes.map((a) => (
                    <AstroListElement
                        key={a.id}
                        astronaute={a}
                        updateHandler={openUpdatePopupHandler}
                        deleteHandler={deleteHandler} />
                ))}
                <AstroCreateItem openCreatePopupHandler={openCreatePopupHandler} />
            </ul>
            <Popup isOpen={isPopupCreateOpen} closeHandler={closeCreatePopupHandler}>
                <AstronauteCreateForm closeHandler={closeCreatePopupHandler} />
            </Popup>
            <Popup isOpen={isPopupUpdateOpen} closeHandler={closeUpdatePopupHandler}>
                <AstronauteUpdateForm updatingAstronauteId={updatingAstronauteId} closeHandler={closeUpdatePopupHandler} />
            </Popup>
        </>
    )
}

const AstroListElement = ({ astronaute, deleteHandler, updateHandler }: AstroListElementProps) => {
    const { name, favoriteCatColor, id } = astronaute;

    return (
        <>
            <li key={id} className='astrolist-item'>
                <span className='astrolist-item-title'>{name}</span>
                <span className='astrolist-item-subtitle'>{favoriteCatColor}</span>

                <div className='astrolist-item-actions'>
                    <button className='astrolist-item-button--plain' onClick={() => updateHandler(id)}>Update</button>
                    <button onClick={() => deleteHandler(id)}>Remove</button>
                </div>
            </li>
        </>
    )
}

const AstroCreateItem = ({ openCreatePopupHandler }: AstroCreateItemProps) => {
    return (
        <li key="register-action" className='astrolist-item astrolist-item--plain' style={{ justifyContent: 'center' }}>
            <span>Register new Astronaute ?</span>
            <button onClick={() => openCreatePopupHandler()}>Register</button>
        </li>
    )
}

type AstroListElementProps = {
    astronaute: AstronauteDTO,
    updateHandler: (id: number) => void
    deleteHandler: (id: number) => void
}

type AstroCreateItemProps = {
    openCreatePopupHandler: () => void
}


export default Astronaute
