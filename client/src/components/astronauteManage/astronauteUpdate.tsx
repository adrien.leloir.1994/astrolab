import { useContext, useState } from "react";
import { DataContextType, DataContext } from "../core/dataprovider";

export const AstronauteUpdateForm = ({ closeHandler, updatingAstronauteId }: AstronauteUpdatePopProps) => {
    const { updateAstronaute, astronautes } = useContext<DataContextType>(DataContext);

    const astronauteToUpdate = astronautes.find((a) => a.id === updatingAstronauteId);

    if (!astronauteToUpdate) {
        closeHandler();

        return null;
    }

    const [name, setName] = useState(astronauteToUpdate?.name);
    const [favoriteCatColor, setFavoriteCatColor] = useState(astronauteToUpdate?.favoriteCatColor);

    const handleNameChange = (event: any) => {
        setName(event.target.value);
    };

    const handleFavoriteColorChange = (event: any) => {
        setFavoriteCatColor(event.target.value);
    };

    const handleSubmit = () => {
        updateAstronaute(astronauteToUpdate.id, {
            name,
            favoriteCatColor,
        })

        closeHandler();

        setName('');
        setFavoriteCatColor('');
    };

    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '10px',
            marginBottom: '10px',
        }}>
            <input style={{ padding: '5px' }} type="text" value={name} onChange={handleNameChange} placeholder='Name' />
            <input style={{ padding: '5px' }} type="text" value={favoriteCatColor} onChange={handleFavoriteColorChange} placeholder='Favorite cat color' />

            <button onClick={handleSubmit}>Update</button>
        </div>
    );
}


type AstronauteUpdatePopProps = {
    closeHandler: () => void;
    updatingAstronauteId: number | null
}
