import { useContext, useState } from "react";
import { DataContextType, DataContext } from "../core/dataprovider";

export const AstronauteCreateForm = ({ closeHandler }: AstronauteCreatePopProps) => {
    const { createAstronaute } = useContext<DataContextType>(DataContext);

    const [name, setName] = useState('');
    const [favoriteCatColor, setFavoriteCatColor] = useState('');

    const handleNameChange = (event: any) => {
        setName(event.target.value);
    };

    const handleFavoriteColorChange = (event: any) => {
        setFavoriteCatColor(event.target.value);
    };

    const handleSubmit = () => {
        createAstronaute({
            name,
            favoriteCatColor,
        })

        closeHandler();

        setName('');
        setFavoriteCatColor('');
    };

    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '10px',
            marginBottom: '10px',
        }}>
            <input style={{ padding: '5px' }} type="text" value={name} onChange={handleNameChange} placeholder='Name' />
            <input style={{ padding: '5px' }} type="text" value={favoriteCatColor} onChange={handleFavoriteColorChange} placeholder='Favorite cat color' />

            <button onClick={handleSubmit}>Register</button>
        </div>
    );
}

type AstronauteCreatePopProps = {
    closeHandler: () => void
}
