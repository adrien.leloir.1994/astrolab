export interface AstronauteDTO {
    id: number;
    name: string;
    favoriteCatColor: string;
}